<?php

namespace supervillainhq\smee\hooks\bitbucket\payloads {

	/**
	 * Created by ak.
	 */
	class BitBucketPayload {
		// 131.103.20.160/27
		// 165.254.145.0/26
		// 104.192.143.0/24

		static $expectedHeaders = [
			//	The event key of the event that triggers the webhook (for example, repo:push).
			'X-Event-Key',
			//	The UUID of the webhook that an event triggers. For details about the UUID of a webhook, see the webhook Resource.
			'X-Hook-UUID',
			//	The UUID of the request.
			'X-Request-UUID',
			// The number of times Bitbucket attempts to send the payload. After the first request, Bitbucket tries to send the payload two more times if the previous attempts fail. The number of attempts also includes those anyone makes from the Resend Request button.
			'X-Attempt-Number'
		];
	}
}
