<?php

namespace supervillainhq\smee\hooks\bitbucket\payloads {

	/**
	 * Created by ak.
	 */
	class RepositoryFork extends BitBucketPayload{
		static $format = <<<FORMAT
{
  "actor": User,
  "repository": Repository,
  "fork": Repository
}
FORMAT;

	}
}
