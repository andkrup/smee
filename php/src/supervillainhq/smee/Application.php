<?php

namespace supervillainhq\smee {
	use supervillainhq\core\db\DataAware;

	/**
	 * Describes a website or web application on the server.
	 *
	 * Created by ak.
	 */
	class Application {
		use DataAware;

		private $name;
		private $installPath;
		private $repoUri;
		private $dataSources;
		private $services; // available services that may need to be executed; gulp, bower, npm, composer, nicknack, etc
		private $hooks; // current external web hooks that may execute services for this application
	}
}
