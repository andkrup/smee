grant all privileges on *.* to 'root'@'%' with grant option;
flush privileges;
create database if not exists Smee default character set utf8 default collate utf8_general_ci;
grant all privileges on Smee.* to 'smee'@'localhost';
set password for 'smee'@'localhost' = password('smee');
