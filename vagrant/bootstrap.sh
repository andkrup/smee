#!/usr/bin/env bash

if [[ -d /vagrant/.apache ]]
	then
	rm -rf /vagrant/.apache
fi
if [[ -d /vagrant/.xdebug ]]
	then
	rm -rf /vagrant/.xdebug
fi
mkdir -p /vagrant/.xdebug /vagrant/.apache

## fix missing locales for ssh session
if ! locale -a | grep -q ^en_US
	then
	echo "generating en_US.UTF-8"
	locale-gen en_US.UTF-8
fi

sed -i '/AcceptEnv LANG LC_*/c\#AcceptEnv LANG LC_*' /etc/ssh/sshd_config

if ! grep -Fxq 'export LC_ALL="en_US.UTF-8"' /home/vagrant/.bashrc
	then
	echo 'export LC_ALL="en_US.UTF-8"' >> /home/vagrant/.bashrc
fi
if ! grep -Fxq 'export LANGUAGE="en_US.UTF-8"' /home/vagrant/.bashrc
	then
	echo 'export LANGUAGE="en_US.UTF-8"' >> /home/vagrant/.bashrc
fi
if ! grep -Fxq 'export LANG="en_US.UTF-8"' /home/vagrant/.bashrc
	then
	echo 'export LANG="en_US.UTF-8"' >> /home/vagrant/.bashrc
fi
if ! grep -Fxq 'export LC_MESSAGES="en_US.UTF-8"' /home/vagrant/.bashrc
	then
	echo 'export LC_MESSAGES="en_US.UTF-8"' >> /home/vagrant/.bashrc
fi

# add required repos (phalcon)
#apt-get install python-software-properties
apt-add-repository ppa:phalcon/stable -y
apt-add-repository ppa:chris-lea/redis-server -y
apt-get update

# set mysql root password for scripted mysql install
debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'

# download and install required software
apt-get install apache2 php5 git php5-xdebug php5-mysql mysql-server php5-phalcon php5-curl mongodb php5-mongo redis-server nodejs npm -y
apt-get autoremove -y

# disable xdebug in php cli
if [[ -L /etc/php5/cli/conf.d/20-xdebug.ini ]]
	then
	echo "disabling php-xdebug in cli-mode"
	unlink /etc/php5/cli/conf.d/20-xdebug.ini
fi

# download & install composer
#cd /usr/local/bin
if [[ ! -f /usr/local/bin/composer ]]
	then
	echo "downloading & installing composer"
	curl -sS https://getcomposer.org/installer | php -- --install-dir=/home/vagrant
	mv /home/vagrant/composer.phar /usr/local/bin/composer
	chmod +x /usr/local/bin/composer
fi

# fix missing node symlink
if [[ ! -L /usr/local/bin/node ]]
	then
	echo "updating node symlink"
	ln -s /usr/bin/nodejs /usr/local/bin/node
fi
# install bower, gulp & stylus
if [[ ! -f /usr/local/bin/bower ]]
	then
	echo "downloading & installing bower, gulp & stylus"
	npm install -g bower gulp stylus
	# set up gulp in project
	cd /vagrant_root/
	npm install --save-dev gulp gulp-stylus
fi

# enable apache modules
a2enmod rewrite

# replace default documentroot with project www root
rm -rf /var/www/html
ln -fs /vagrant_root/public /var/www/html

# replace /var/log/apache2 with symlink to vagrant folder
rm -rf /var/log/apache2
ln -fs /vagrant/.apache /var/log/apache2

# pointing enabled default configuration to the one in our repository
ln -fs /vagrant/default.conf /etc/apache2/sites-enabled/000-default.conf
# add our custom php settings
ln -fs /vagrant/php.ini /etc/php5/apache2/conf.d/customsettings.ini

# install composer dependencies
cd /vagrant_root
composer install

# export the thug scripts
if [[ ! -L /usr/bin/nicknack ]]
	then
	echo "update the thug nicknack symlinks"
	ln -s /vagrant_root/vendor/bin/nicknack /usr/bin/nicknack
	chmod +x /usr/bin/nicknack
fi
if [[ ! -L /usr/bin/oddjob ]]
	then
	echo "update the thug oddjob symlinks"
	ln -s /vagrant_root/vendor/bin/oddjob /usr/bin/oddjob
	chmod +x /usr/bin/oddjob
fi

# install/configure phalcon devtools
if [[ ! -L /usr/bin/phalcon ]]
	then
	echo "updating phalcon devtools symlink"
	ln -s /vagrant_root/vendor/phalcon/devtools/phalcon.php /usr/bin/phalcon
	chmod +x /usr/bin/phalcon
fi

# re-establish database with user @ 'localhost'
mysql -uroot -pvagrant < init.sql

service apache2 restart
