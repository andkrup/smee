<?php
namespace dashboard\controllers{
	use supervillainhq\spectre\cms\controllers\CmsController;

	class HooksController extends CmsController{

		function initialize(){
			parent::initialize();
			$this->gettext->init();
		}

		public function indexAction(){
			$this->requireHeadScripts(['mustache', 'lightslider']);
			$this->requireStylesheets(['lightslider']);
		}

		public function createAction(){
			$this->requireHeadScripts(['mustache', 'lightslider']);
			$this->requireStylesheets(['lightslider']);
		}
	}
}

