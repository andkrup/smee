<?php
namespace dashboard\controllers{
	use supervillainhq\spectre\cms\controllers\CmsController;

	class ApplicationsController extends CmsController{

		function initialize(){
			parent::initialize();
			$this->gettext->init();
			// use /js/dashboard.js in the views created by all actions
			$this->requireHeadScripts(['dashboard']);
		}

		public function indexAction(){
		}

		public function createAction(){
		}
	}
}

