<?php

namespace supervillainhq\smee {
	use Phalcon\DiInterface;
	use Phalcon\Loader;
	use Phalcon\Mvc\Dispatcher;
	use Phalcon\Mvc\ModuleDefinitionInterface;
	use Phalcon\Mvc\View;

	/**
	 * Created by ak.
	 */
	class SmeeDashboard implements ModuleDefinitionInterface{
		private $session;

		function __construct(){
		}

		public function registerAutoloaders(DiInterface $dependencyInjector=null){
			$loader = new Loader();
			$loader->registerNamespaces([
					'dashboard\controllers' => '../app/dashboard/controllers/',
			]);
			$loader->register();
		}
		public function registerServices(DiInterface $dependencyInjector){
			//Registering a dispatcher
			$dependencyInjector->set('dispatcher', function() {
				$dispatcher = new Dispatcher();
				$dispatcher->setDefaultNamespace('dashboard\controllers');
				return $dispatcher;
			});

			//Registering the view component
			$dependencyInjector->set('view', function() {
				$view = new View();
				$view->setViewsDir('../app/dashboard/views/');
				return $view;
			});
		}
	}
}
