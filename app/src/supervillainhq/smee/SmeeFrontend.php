<?php
namespace supervillainhq\smee{
	use Phalcon\Mvc\ModuleDefinitionInterface;
	use Phalcon\Loader;
	use Phalcon\Mvc\View;
	use Phalcon\Mvc\Dispatcher;

	class SmeeFrontend implements ModuleDefinitionInterface{
		private $session;

		function __construct(){
		}

		public function registerAutoloaders(\Phalcon\DiInterface $dependencyInjector=null){
			$loader = new Loader();
			$loader->registerNamespaces([
					'frontend\controllers' => '../app/frontend/controllers/',
			]);
			$loader->register();
		}
		public function registerServices(\Phalcon\DiInterface $dependencyInjector){
			//Registering a dispatcher
			$dependencyInjector->set('dispatcher', function() {
				$dispatcher = new Dispatcher();
				$dispatcher->setDefaultNamespace('frontend\controllers');
				return $dispatcher;
			});

			//Registering the view component
			$dependencyInjector->set('view', function() {
				$view = new View();
				$view->setViewsDir('../app/frontend/views/');
				return $view;
			});
		}
	}
}