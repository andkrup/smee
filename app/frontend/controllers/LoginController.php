<?php
namespace frontend\controllers{
	use supervillainhq\spectre\cms\controllers\CmsController;

	class LoginController extends CmsController{

		public function indexAction(){
		}

		public function authAction(){
			$token = $this->security->checkToken();
			if($token && $this->request->isPost()){
				if($this->request->has('email') && $this->request->has('password')){
					$email = $this->request->get('email');
					$pass = $this->request->get('password');
					if($this->auth->authenticate($email, $pass)){
						if($this->auth->isAdmin()){
							$this->response->redirect('/dashboard');
						}
						else{
							$this->response->redirect('/profile');
						}
					}
					return;
				}
			}
			$this->auth->signOut();
			$this->response->redirect('/');
		}

	}
}

