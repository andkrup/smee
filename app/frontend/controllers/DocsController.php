<?php
namespace frontend\controllers{
	use supervillainhq\spectre\cms\controllers\CmsController;

	class DocsController extends CmsController{

		function initialize(){
			parent::initialize();
			$this->gettext->init();
		}

		public function indexAction(){
			$this->requireHeadScripts(['mustache', 'lightslider']);
			$this->requireStylesheets(['lightslider']);
		}
	}
}

