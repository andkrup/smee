var argv = require('yargs').argv;
var gulp = require('gulp');
var config = require('./gulp.json');
var stylus = require('gulp-stylus');
var replace = require('gulp-replace');
var rename = require('gulp-rename');
var rsync = require('gulp-rsync');
var browserify = require('browserify');
var source = require('vinyl-source-stream');

gulp.task('dependencies', function () {
	gulp.src(config.bowerpath + '/jquery/jquery.min.js').pipe(gulp.dest('public/js/'));
	gulp.src(config.bowerpath + '/jquery-ui/jquery-ui.min.js').pipe(gulp.dest('public/js/'));
	gulp.src(config.bowerpath + '/jquery-ui/ui/minified/slider.min.js').pipe(gulp.dest('public/js/'));
	gulp.src(config.bowerpath + '/jquery-ui/ui/minified/datepicker.min.js').pipe(gulp.dest('public/js/'));
	gulp.src(config.bowerpath + '/jqueryui-timepicker-addon/src/jquery-ui-timepicker-addon.js').pipe(gulp.dest('public/js/'));
	gulp.src(config.bowerpath + '/jquery-colorbox/jquery.colorbox-min.js').pipe(gulp.dest('public/js/'));
	gulp.src(config.bowerpath + '/jquery-treetable/jquery.treetable.js').pipe(gulp.dest('public/js/'));
	gulp.src(config.bowerpath + '/mustache.js/mustache.min.js').pipe(gulp.dest('public/js/'));
	gulp.src(config.bowerpath + '/trumbowyg/dist/trumbowyg.min.js').pipe(gulp.dest('public/js/'));
	
	gulp.src(config.bowerpath + '/HTML5-Reset/assets/css/reset.css').pipe(gulp.dest('public/css/'));
	gulp.src(config.bowerpath + '/jquery-colorbox/example1/colorbox.css').pipe(gulp.dest('public/css/'));
	gulp.src(config.bowerpath + '/jquery-colorbox/example1/images/*').pipe(gulp.dest('public/css/images/'));
	gulp.src(config.bowerpath + '/jquery-treetable/css/jquery.treetable.css').pipe(gulp.dest('public/css/'));
	gulp.src(config.bowerpath + '/jquery-treetable/css/jquery.treetable.theme.default.css').pipe(gulp.dest('public/css/'));
	gulp.src(config.bowerpath + '/jqueryui-timepicker-addon/src/jquery-ui-timepicker-addon.css').pipe(gulp.dest('public/css/'));
	gulp.src(config.bowerpath + '/jquery-ui/themes/cupertino/jquery-ui.min.css').pipe(gulp.dest('public/css/'));
	gulp.src(config.bowerpath + '/trumbowyg/dist/ui/trumbowyg.min.css').pipe(gulp.dest('public/css/'));
	
	gulp.src(config.bowerpath + '/trumbowyg/dist/ui/images/*').pipe(gulp.dest('public/css/images/'));
	gulp.src(config.bowerpath + '/jquery-ui/themes/cupertino/images/*').pipe(gulp.dest('public/css/images/'));

	gulp.src(config.bowerpath + '/Font-Awesome-Stylus/fonts/*').pipe(gulp.dest('public/fonts/'));
});

gulp.task('resources', function () {
	gulp.src(config.resourcepath + '/images/**/*', {base:config.resourcepath + '/images'}).pipe(gulp.dest('./public/images'));
});

gulp.task('styles', function () {
	gulp.src(config.resourcepath + '/fonts.css').pipe(gulp.dest('./public/css'));
	gulp.src(config.resourcepath + '/stylus/default.styl').pipe(stylus()).pipe(gulp.dest('./public/css'));
});

gulp.task('dashboard', function () {
	var bundleStream = browserify(config.sourcepath + '/dashboard/dashboard.js').bundle();

	bundleStream
    .pipe(source('dashboard.js'))
    // .pipe(rename('bundle.js'))
    .pipe(gulp.dest('./public/js'))
});

gulp.task('environment', function () {
	var src;
	switch(argv.env){
		case 'vagrant':
			gulp.src(config.environments.vagrant.src + '/config/*.json').pipe(gulp.dest('./env/'));
			break;
		case 'test':
			gulp.src(config.environments.test.src + '/config/*.json').pipe(gulp.dest('./env/'));
			break;
		default:
			console.log('set up for unknown');
			break;
//		var databaseConfig = require('./env/db.json');
//		console.log(databaseConfig.adapter);
	}
});

gulp.task('default', ['dependencies', 'styles']);