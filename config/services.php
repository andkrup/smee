<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Security;
use Phalcon\Flash\Session as FlashSession;
use supervillainhq\henchmen\gettext\Gettext;
use supervillainhq\spectre\auth\MySqlAuthenticator;
use supervillainhq\spectre\auth\UserManager;
use supervillainhq\spectre\cms\CmsClient;
use supervillainhq\spectre\cms\Site;
use supervillainhq\spectre\cms\assets\UploadManager;
use supervillainhq\spectre\cms\components\FrontpageSlider;
use supervillainhq\spectre\cms\PhalconCms;
use supervillainhq\spectre\ObjectEncoder;
use supervillainhq\spectre\DataObjectFactory;
use supervillainhq\smee\Smee;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

$di->set('config', function () use ($config) {
	return $config;
}, true);

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
}, true);

/**
 * Setting up the view component
 */
$di->set('view', function () use ($config) {

    $view = new View();

    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines(array(
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ));

    return $view;
}, true);


/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () use ($config) {
	return new DbAdapter(array(
		'host' => $config->database->host,
		'username' => $config->database->username,
		'password' => $config->database->password,
		'dbname' => $config->database->dbname,
		"charset" => $config->database->charset
	));
});

$di->set('objectmapper', function ($type) use($di){
	$parameters = func_get_args();
	array_shift($parameters);
	$map = include dirname(__FILE__) . '/classmap.php';
	$factory = new DataObjectFactory($map, $di);
	$factory->addMappers(PhalconCms::getCmsMappers()); // also use the cms mappers
	return $factory->getMapper($type, $parameters);
});
$di->set('objectwriter', function ($type) use($di){
	$parameters = func_get_args();
	array_shift($parameters);
	$map = include dirname(__FILE__) . '/classmap.php';
	$factory = new DataObjectFactory($map, $di);
	$factory->addWriters(PhalconCms::getCmsWriters()); // also use the cms writers
	return $factory->getWriter($type, $parameters);
});

$di->set('router', function () {
	$router = new Router();
	$router->setDefaultModule("frontend");
	// requesting a path with one child directory from root should resolve as a dynamic cms page
	$router->add(
			'/{params:[a-z0-9-_]+}.evl',
			[
					'module' => 'cms',
					'controller' => 'cms',
					'action' => 'bySlug',
					'params' => 1
			]
	);
	$router->add(
			'/dashboard/:params',
			[
					'module' => 'dashboard',
					'controller' => 'index'
			]
	);
	$router->add(
			"/dashboard/:action/:params",
			[
					"module" => "dashboard",
					"controller" => 1,
					"action"     => 'index',
					"params"     => 2,
			]
	);
	$router->add(
			"/dashboard/:controller/:action/:params",
			[
					"module" => "dashboard",
					"controller" => 1,
					"action"     => 2,
					"params"     => 3,
			]
	);
	$router->add(
			"/hooks/{service:[a-z0-9]*}/{appkey:[a-z0-9]*}/{hook:[a-z0-9]*}/:params",
			[
					"module" => "api",
					"controller" => 'hooks',
					"action"     => 'index',
					"params"     => 3,
			]
	);
	// requesting an html file in root should be resolved by the indexcontroller as a static page with view file
	$router->add(
			'/{action:[a-z0-9]*}\.hook',
			[
					'module' => 'api',
					'controller' => 'index',
					'action' => 1
			]
	);
	$router->add(
			'/:controller/{action:[a-z0-9]*}\.hook',
			[
					'module' => 'api',
					'controller' => 1,
					'action' => 2
			]
	);
	return $router;
}, true);

$di->set('dispatcher', function() use ($di) {
	$eventsManager = $di->getShared('eventsManager');
	$client = $di->get('cmsclient');
	$eventsManager->attach('application', $client);
	$eventsManager->attach('dispatch', $client);
	$dispatcher = new Dispatcher();
	$dispatcher->setEventsManager($eventsManager);
	return $dispatcher;
});

// Set up the flash service
$di->set('flash', function () {
    return new FlashSession();
});

// Frontend client object
$di->set('cmsclient', function() use($config){
	$cmsClient = new CmsClient();
	return $cmsClient;
}, true);

// scripts/styles list
$di->set('site', function() {
	return Site::instance();
}, true);

// json encoder
$di->set('objectencoder', function($value) use($di){
	return new ObjectEncoder($di, $value);
}, true);

$di->set('encoderfactory', function($value){
	// implement a supervillainhq\core\Encoder
// 	$factory = new FishingJournalEncoderFactory($value);
// 	return $factory->create();
	return null;
});

// Auth helper
$di->set('auth', function() use ($di){
	$security = $di->get('security');
	$session = $di->get('session');
	return new MySqlAuthenticator($security, $session, $di);
}, true);
$di->set('userManager', function () use ($di, $config) {
	return new UserManager($di, $config);
});


$di->set('frontpageSlider', function () use ($di, $config) {
	return new FrontpageSlider($di, $config);
});

// $di->set('blog', function () use ($di, $config) {
// 	$blog = new Blog($di, $config);
// 	return $blog;
// });

$di->set('uploader', function() use ($di, $config){
	return new UploadManager($config->files);
});

$di->set('gettext', function() use ($di, $config){
	return new Gettext($config->gettext, $config->locale);
});

$di->set('smee', function() use ($di, $config){
	return new Smee($di, $config->smee, $config->locale);
});

$di->set('security', function () {
	$security = new Security();
	// Set the password hashing factor to 12 rounds
	$security->setWorkFactor(12);
	return $security;
}, true);

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
	$session = new SessionAdapter([
			'uniqueId' => '2-bissen-web'
	]);
	$session->start();

	return $session;
});
