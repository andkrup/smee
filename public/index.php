<?php
# composer autoload
include __DIR__ . '/../vendor/autoload.php';

use supervillainhq\spectre\db\SqlQuery;
use supervillainhq\core\date\Date;

error_reporting(E_ALL);



try {

    /**
     * Read the configuration
     */
    $config = include __DIR__ . "/../config/config.php";

    /**
     * Read auto-loader
     */
    include __DIR__ . "/../config/loader.php";

    /**
     * Read services
     */
    include __DIR__ . "/../config/services.php";

    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);

	$application->registerModules(
		[
			'frontend' => [
				'className' => 'supervillainhq\smee\SmeeFrontend',
				'path'      => '../app/src/supervillainhq/smee/SmeeFrontend.php',
			],
			'api'  => [
				'className' => 'supervillainhq\smee\SmeeAPI',
				'path'      => '../app/src/supervillainhq/smee/SmeeAPI.php',
			],
			'dashboard'  => [
				'className' => 'supervillainhq\smee\SmeeDashboard',
				'path'      => '../app/src/supervillainhq/smee/SmeeDashboard.php',
			]
		]
	);
    // static reference to the current dbadapter
    SqlQuery::$dbAdapter = $application->db;
    // set up datetimeutil
    Date::config((array) $config->locale);

    echo $application->handle()->getContent();

} catch (\Exception $e) {
    echo $e->getMessage();
}
